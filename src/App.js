import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import createAction from './store/createAction'
import { ACTIONS_TYPES } from './store/action-types'

import './app.sass'

const App = () => {

  const dispatch = useDispatch()
  const { todos } = useSelector(state => state)

  const [value, setValue] = useState("")
  const [editMode, setEditMode] = useState(null)
  const [editingTodoId, setEditingTodoId] = useState(null)

  const addTodo = () => {
    if (value === "") return
    dispatch(createAction(
      ACTIONS_TYPES.ADD_TODO,
      {
        id: Math.random(),
        text: value,
        completed: false
      }
    ))
    setValue("")
  }

  const editTodo = (id, text) => {
    setEditMode(true)
    setEditingTodoId(id)
    setValue(text)
  }

  const updateTodo = () => {
    dispatch(createAction(ACTIONS_TYPES.UPDATE_TODO, { id: editingTodoId, text: value }))
    setEditMode(false)
    setValue("")
  }

  const deleteTodo = (id) => {
    dispatch(createAction(ACTIONS_TYPES.DELETE_TODO, { id: id }))
  }

  const setCompletedTodo = (id) => {
    dispatch(createAction(ACTIONS_TYPES.SET_COMPLETE_TODO, { id: id }))
  }

  const clearCompleted = () => {
    dispatch(createAction(ACTIONS_TYPES.CLEAR_COMPLETED, null))
  }

  const onEnterDown = (e) => e.code === "Enter" && addTodo()

  return (
    <div className="todos">
      <h1>Todo List with React {"&"} Redux</h1>
      <div className="input-todo">
        <input type="text" value={value} onKeyDown={onEnterDown} onChange={(e) => setValue(e.target.value)} placeholder="type..." />
        <p>{todos.filter(({completed}) => completed === true).length}/{todos.length}</p>
        {editMode
          ? <button onClick={updateTodo}>Update</button>
          : <>
            <button onClick={addTodo}>Add</button>
            <button onClick={clearCompleted}>Clear Completed</button>
          </>}
      </div>
      <div className="todos-list">
        {todos.length === 0 ? <h2>List is empty</h2> : todos.map(({ text, id, completed }, i) => {
          return <div className="todo" key={i}>
            <div>
              <input type="checkbox" class="custom-checkbox" id={id} name={id} checked={completed} onChange={() => setCompletedTodo(id)} />
              <label for={id}></label>
              <p>{text}</p>
            </div>
            <div>
              <button onClick={() => editTodo(id, text)}>Edit</button>
              <button onClick={() => deleteTodo(id)}>Delete</button>
            </div>
          </div>
        })}
      </div>
    </div>
  )
}

export default App
