import { legacy_createStore as createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import { ACTIONS_TYPES } from './action-types'

const INITIAL_STATE = {
    todos: []
}

const todosReducer = (state = INITIAL_STATE, action) => {
    const { todos } = state
    const { type, payload } = action
    switch (type) {
        case ACTIONS_TYPES.ADD_TODO:
            return {
                ...state,
                todos: [
                    {
                        id: payload.id,
                        text: payload.text,
                        completed: payload.completed
                    },
                    ...todos
                ]
            }
        case ACTIONS_TYPES.UPDATE_TODO:
            return {
                ...state,
                todos: todos.map(el => el.id === payload.id ? { ...el, text: payload.text } : el)
            }
        case ACTIONS_TYPES.DELETE_TODO:
            return {
                ...state,
                todos: todos.filter(({ id }) => id !== payload.id)
            }
        case ACTIONS_TYPES.SET_COMPLETE_TODO:
            return {
                ...state,
                todos: todos.map(el => el.id === payload.id ? { ...el, completed: !el.completed } : el)
            }
        case ACTIONS_TYPES.CLEAR_COMPLETED:
            return {
                ...state,
                todos: todos.filter(({ completed }) => completed === false)
            }
        default:
            return state
    }
}

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['user']
}

const persistedReducer = persistReducer(persistConfig, todosReducer)

export const store = createStore(persistedReducer)

export const persistor = persistStore(store)